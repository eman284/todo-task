function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
  /***/
  "./$$_lazy_route_resource lazy recursive":
  /*!******************************************************!*\
    !*** ./$$_lazy_route_resource lazy namespace object ***!
    \******************************************************/

  /*! no static exports found */

  /***/
  function $$_lazy_route_resourceLazyRecursive(module, exports) {
    function webpackEmptyAsyncContext(req) {
      // Here Promise.resolve().then() is used instead of new Promise() to prevent
      // uncaught exception popping up in devtools
      return Promise.resolve().then(function () {
        var e = new Error("Cannot find module '" + req + "'");
        e.code = 'MODULE_NOT_FOUND';
        throw e;
      });
    }

    webpackEmptyAsyncContext.keys = function () {
      return [];
    };

    webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
    module.exports = webpackEmptyAsyncContext;
    webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
    /***/
  },

  /***/
  "./src/app/app.component.ts":
  /*!**********************************!*\
    !*** ./src/app/app.component.ts ***!
    \**********************************/

  /*! exports provided: AppComponent */

  /***/
  function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
      return AppComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _to_do_to_do_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./to-do/to-do.component */
    "./src/app/to-do/to-do.component.ts");

    var AppComponent = function AppComponent(translate) {
      _classCallCheck(this, AppComponent);

      this.translate = translate;
      translate.addLangs(['en', 'ar']);
      translate.setDefaultLang('en');
    };

    AppComponent.ɵfac = function AppComponent_Factory(t) {
      return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__["TranslateService"]));
    };

    AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: AppComponent,
      selectors: [["app-root"]],
      decls: 2,
      vars: 0,
      consts: [[1, "container"]],
      template: function AppComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-to-do");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      },
      directives: [_to_do_to_do_component__WEBPACK_IMPORTED_MODULE_2__["ToDoComponent"]],
      styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-root',
          templateUrl: './app.component.html',
          styleUrls: ['./app.component.scss']
        }]
      }], function () {
        return [{
          type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__["TranslateService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/app.module.ts":
  /*!*******************************!*\
    !*** ./src/app/app.module.ts ***!
    \*******************************/

  /*! exports provided: createTranslateLoader, AppModule */

  /***/
  function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "createTranslateLoader", function () {
      return createTranslateLoader;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppModule", function () {
      return AppModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/platform-browser/animations */
    "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/animations.js");
    /* harmony import */


    var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ngx-toastr */
    "./node_modules/ngx-toastr/__ivy_ngcc__/fesm2015/ngx-toastr.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/http-loader */
    "./node_modules/@ngx-translate/http-loader/__ivy_ngcc__/fesm2015/ngx-translate-http-loader.js");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _lang_lang_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./lang/lang.component */
    "./src/app/lang/lang.component.ts");
    /* harmony import */


    var ngx_slimscroll__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ngx-slimscroll */
    "./node_modules/ngx-slimscroll/__ivy_ngcc__/fesm2015/ngx-slimscroll.js");
    /* harmony import */


    var _to_do_to_do_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./to-do/to-do.component */
    "./src/app/to-do/to-do.component.ts");

    function createTranslateLoader(http) {
      return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_7__["TranslateHttpLoader"](http, './assets/i18n/', '.json');
    }

    var AppModule = function AppModule() {
      _classCallCheck(this, AppModule);
    };

    AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: AppModule,
      bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]]
    });
    AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function AppModule_Factory(t) {
        return new (t || AppModule)();
      },
      providers: [],
      imports: [[_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrModule"].forRoot(), _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateModule"].forRoot({
        loader: {
          provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateLoader"],
          useFactory: createTranslateLoader,
          deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]]
        }
      }), ngx_slimscroll__WEBPACK_IMPORTED_MODULE_10__["NgSlimScrollModule"]], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateModule"]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppModule, {
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"], _lang_lang_component__WEBPACK_IMPORTED_MODULE_9__["LangComponent"], _to_do_to_do_component__WEBPACK_IMPORTED_MODULE_11__["ToDoComponent"]],
        imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateModule"], ngx_slimscroll__WEBPACK_IMPORTED_MODULE_10__["NgSlimScrollModule"]],
        exports: [_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          declarations: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"], _lang_lang_component__WEBPACK_IMPORTED_MODULE_9__["LangComponent"], _to_do_to_do_component__WEBPACK_IMPORTED_MODULE_11__["ToDoComponent"]],
          imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrModule"].forRoot(), _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateModule"].forRoot({
            loader: {
              provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateLoader"],
              useFactory: createTranslateLoader,
              deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]]
            }
          }), ngx_slimscroll__WEBPACK_IMPORTED_MODULE_10__["NgSlimScrollModule"]],
          exports: [_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateModule"]],
          providers: [],
          bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/lang/lang.component.ts":
  /*!****************************************!*\
    !*** ./src/app/lang/lang.component.ts ***!
    \****************************************/

  /*! exports provided: LangComponent */

  /***/
  function srcAppLangLangComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LangComponent", function () {
      return LangComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    function LangComponent_a_1_Template(rf, ctx) {
      if (rf & 1) {
        var _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function LangComponent_a_1_Template_a_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3);

          var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r2.changeLang("en", "ltr");
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "en");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function LangComponent_a_2_Template(rf, ctx) {
      if (rf & 1) {
        var _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function LangComponent_a_2_Template_a_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5);

          var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r4.changeLang("ar", "rtl");
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "ar");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    var LangComponent = /*#__PURE__*/function () {
      function LangComponent(translate) {
        _classCallCheck(this, LangComponent);

        this.translate = translate;
      }

      _createClass(LangComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.defaultLang = this.translate.getDefaultLang();
        }
      }, {
        key: "changeLang",
        value: function changeLang(lang, dir) {
          this.translate.setDefaultLang(lang);
          document.querySelector('html').setAttribute('dir', dir);
          document.querySelector('html').setAttribute('lang', lang);
          this.defaultLang = lang;
        }
      }]);

      return LangComponent;
    }();

    LangComponent.ɵfac = function LangComponent_Factory(t) {
      return new (t || LangComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__["TranslateService"]));
    };

    LangComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: LangComponent,
      selectors: [["app-lang"]],
      decls: 3,
      vars: 2,
      consts: [[1, "lang-container"], ["class", "change-lang", 3, "click", 4, "ngIf"], [1, "change-lang", 3, "click"]],
      template: function LangComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, LangComponent_a_1_Template, 2, 0, "a", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, LangComponent_a_2_Template, 2, 0, "a", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.defaultLang === "ar");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.defaultLang === "en");
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"]],
      styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xhbmcvbGFuZy5jb21wb25lbnQuc2NzcyJ9 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LangComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-lang',
          templateUrl: './lang.component.html',
          styleUrls: ['./lang.component.scss']
        }]
      }], function () {
        return [{
          type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_1__["TranslateService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/list/list-service.service.ts":
  /*!**********************************************!*\
    !*** ./src/app/list/list-service.service.ts ***!
    \**********************************************/

  /*! exports provided: ListService */

  /***/
  function srcAppListListServiceServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ListService", function () {
      return ListService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var ListService = /*#__PURE__*/function () {
      function ListService(httpClient) {
        _classCallCheck(this, ListService);

        this.httpClient = httpClient;
        this.apiURL = 'https://my-json-server.typicode.com/eman284/list-todo';
        this.httpOptions = {
          headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
            'Content-Type': 'application/json'
          })
        };
      }

      _createClass(ListService, [{
        key: "getAll",
        value: function getAll() {
          return this.httpClient.get(this.apiURL + '/list/').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
        }
      }, {
        key: "create",
        value: function create(list) {
          return this.httpClient.post(this.apiURL + '/list/', JSON.stringify(list), this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
        }
      }, {
        key: "find",
        value: function find(id) {
          return this.httpClient.get(this.apiURL + '/list/' + id);
        }
      }, {
        key: "update",
        value: function update(id, list) {
          return this.httpClient.put(this.apiURL + '/list/' + id, JSON.stringify(list), this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
        }
      }, {
        key: "delete",
        value: function _delete(id) {
          return this.httpClient["delete"](this.apiURL + '/list/' + id, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(this.errorHandler));
        }
      }, {
        key: "errorHandler",
        value: function errorHandler(error) {
          var errorMessage = '';

          if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
          } else {
            errorMessage = "Error Code: ".concat(error.status, "\nMessage: ").concat(error.message);
          }

          return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(errorMessage);
        }
      }]);

      return ListService;
    }();

    ListService.ɵfac = function ListService_Factory(t) {
      return new (t || ListService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]));
    };

    ListService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: ListService,
      factory: ListService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ListService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/to-do/to-do.component.ts":
  /*!******************************************!*\
    !*** ./src/app/to-do/to-do.component.ts ***!
    \******************************************/

  /*! exports provided: ToDoComponent */

  /***/
  function srcAppToDoToDoComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ToDoComponent", function () {
      return ToDoComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _list_list_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../list/list-service.service */
    "./src/app/list/list-service.service.ts");
    /* harmony import */


    var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ngx-toastr */
    "./node_modules/ngx-toastr/__ivy_ngcc__/fesm2015/ngx-toastr.js");
    /* harmony import */


    var _lang_lang_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../lang/lang.component */
    "./src/app/lang/lang.component.ts");
    /* harmony import */


    var ngx_slimscroll__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ngx-slimscroll */
    "./node_modules/ngx-slimscroll/__ivy_ngcc__/fesm2015/ngx-slimscroll.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/__ivy_ngcc__/fesm2015/ngx-translate-core.js");

    function ToDoComponent_li_14_Template(rf, ctx) {
      if (rf & 1) {
        var _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "input", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "span", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "a", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ToDoComponent_li_14_Template_a_click_8_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5);

          var i_r3 = ctx.index;
          var item_r2 = ctx.$implicit;

          var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r4.edit(i_r3, item_r2);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "i", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "a", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ToDoComponent_li_14_Template_a_click_10_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r5);

          var item_r2 = ctx.$implicit;

          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r6["delete"](item_r2.id);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "i", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r2 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("checked", item_r2.status);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r2.content);
      }
    }

    function ToDoComponent_div_17_h3_3_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h3");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "translate");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](2, 1, "Shared.Add"));
      }
    }

    function ToDoComponent_div_17_h3_4_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h3");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "translate");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](2, 1, "Shared.Edit"));
      }
    }

    function ToDoComponent_div_17_label_10_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "label", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "translate");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](2, 1, "Shared.AddToDo"));
      }
    }

    function ToDoComponent_div_17_label_11_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "label", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "translate");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](2, 1, "Shared.EditToDo"));
      }
    }

    function ToDoComponent_div_17_div_13_span_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "translate");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](2, 1, "Shared.EnterMsg"));
      }
    }

    function ToDoComponent_div_17_div_13_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, ToDoComponent_div_17_div_13_span_1_Template, 3, 3, "span", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r11.f.content.errors.required);
      }
    }

    function ToDoComponent_div_17_span_22_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "i", 43);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](3, "translate");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](3, 1, "Shared.Save"), "");
      }
    }

    function ToDoComponent_div_17_span_23_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "i", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](3, "translate");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](3, 1, "Shared.Edit"), "");
      }
    }

    var _c0 = function _c0(a0) {
      return [a0];
    };

    function ToDoComponent_div_17_Template(rf, ctx) {
      if (rf & 1) {
        var _r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, ToDoComponent_div_17_h3_3_Template, 3, 3, "h3", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, ToDoComponent_div_17_h3_4_Template, 3, 3, "h3", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ToDoComponent_div_17_Template_a_click_5_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r16);

          var ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r15.closeModal();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "i", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "form", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, ToDoComponent_div_17_label_10_Template, 3, 3, "label", 32);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, ToDoComponent_div_17_label_11_Template, 3, 3, "label", 32);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "input", 33);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, ToDoComponent_div_17_div_13_Template, 2, 1, "div", 34);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "input", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "span", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "span", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](19, "translate");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 37);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "button", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ToDoComponent_div_17_Template_button_click_21_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r16);

          var ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r17.add();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](22, ToDoComponent_div_17_span_22_Template, 4, 3, "span", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, ToDoComponent_div_17_span_23_Template, 4, 3, "span", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r1.editable);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.editable);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx_r1.form);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r1.editable);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.editable);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.f.content.touched && ctx_r1.f.content.invalid);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](19, 11, "Shared.Done"));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !ctx_r1.form.valid)("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](13, _c0, !ctx_r1.editable ? "btn-success" : "btn-primary"));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r1.editable);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r1.editable);
      }
    }

    var ToDoComponent = /*#__PURE__*/function () {
      /***** End Date Variables *****/
      function ToDoComponent(listService, toastr) {
        _classCallCheck(this, ToDoComponent);

        this.listService = listService;
        this.toastr = toastr;
        this.list = [];
        this.editable = false;
        this.open = false;
        this.editList = {};
        this.slimScrollopts = {
          barBackground: '#B2B8BA',
          barOpacity: '0.8',
          barWidth: '5',
          barBorderRadius: '30',
          barMargin: '25px',
          gridOpacity: '1',
          gridWidth: '5',
          gridBorderRadius: '30',
          gridMargin: '25px',
          alwaysVisible: true,
          visibleTimeout: 1500,
          gridBackground: '#E5E7E8'
        };
        /***** Start Date Variables *****/

        this.d = new Date();
        this.monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        this.daysNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        this.month = this.monthNames[this.d.getMonth()];
        this.dayName = this.daysNames[this.d.getDay()];
        this.dayNum = this.d.getUTCDate();
        this.year = this.d.getFullYear();
      }

      _createClass(ToDoComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.form = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            content: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
          });
          this.listService.getAll().subscribe(function (data) {
            _this.list = data;
            console.log(_this.list);
          });

          if (this.editable) {
            this.id = this.editableIndex;
            this.listService.find(this.id).subscribe(function (data) {
              _this.editList = data;
            });
          }
        }
      }, {
        key: "add",

        /***** Start Add Function *****/
        value: function add() {
          var _this2 = this;

          if (!this.editable) {
            this.listService.create(this.form.value).subscribe(function (res) {
              _this2.list.push(res);

              _this2.toastr.success('Add successfully', '', {
                timeOut: 2000
              });
            });
          } else {
            this.editable = false;
            this.editList = {
              content: this.editList.content,
              status: this.editList.status
            };
            this.list.splice(this.editableIndex, 1, this.form.value);
          }

          this.open = false;
          this.form.reset();
        }
        /***** End Add Function *****/

        /***** Start Edit Function *****/

      }, {
        key: "edit",
        value: function edit(i, list) {
          var _this3 = this;

          this.open = true;
          this.editable = true;
          this.editableIndex = i;
          this.listService.update(i, list).subscribe(function (res) {
            _this3.editList = res;

            _this3.form.patchValue({
              content: _this3.editList.content,
              status: _this3.editList.status
            });
          });
        }
        /***** End Edit Function *****/

        /***** Start Delete Function *****/

      }, {
        key: "delete",
        value: function _delete(id) {
          var _this4 = this;

          this.listService["delete"](id).subscribe(function (res) {
            var r = confirm('Are you sure delete item ?');

            if (r === true) {
              _this4.list = _this4.list.filter(function (item) {
                return item.id !== id;
              });

              _this4.toastr.success('Deleted successfully', '', {
                timeOut: 2000
              });
            }
          });
        }
        /***** End Add Function *****/

        /***** Start Close Modal Function *****/

      }, {
        key: "closeModal",
        value: function closeModal() {
          this.open = false;
          this.form.reset();
        }
        /***** End Close Modal Function *****/

        /***** Start Open Modal Function *****/

      }, {
        key: "openModal",
        value: function openModal() {
          this.open = true;
          this.editable = false;
        }
      }, {
        key: "f",
        get: function get() {
          return this.form.controls;
        }
      }]);

      return ToDoComponent;
    }();

    ToDoComponent.ɵfac = function ToDoComponent_Factory(t) {
      return new (t || ToDoComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_list_list_service_service__WEBPACK_IMPORTED_MODULE_2__["ListService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]));
    };

    ToDoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: ToDoComponent,
      selectors: [["app-to-do"]],
      decls: 18,
      vars: 7,
      consts: [[1, "list-container"], [1, "date-details"], [1, "date"], [1, "day"], [1, "details"], [1, "month"], [1, "year"], [1, "day-name"], ["slimScroll", "", 3, "options"], [4, "ngFor", "ngForOf"], [1, "btn-icon", "success", "fixed", "large", "dark", 3, "click"], [1, "la", "la-plus"], ["class", "modal", 4, "ngIf"], [1, "checkbox", "custom-style"], ["type", "checkbox", "disabled", "", 3, "checked"], [1, "check-style"], [1, "text"], [1, "action-container"], [1, "action"], [1, "btn-icon", "primary", 3, "click"], [1, "la", "la-pencil"], [1, "btn-icon", "danger", 3, "click"], [1, "la", "la-trash"], [1, "modal"], [1, "modal-dialog"], [1, "modal-header"], [4, "ngIf"], [1, "btn-icon", "defult", 3, "click"], [1, "la", "la-times"], [1, "modal-body"], [3, "formGroup"], [1, "form-group"], ["class", "label-control", 4, "ngIf"], ["formControlName", "content", "id", "content", "type", "text", 1, "form-control"], ["class", "alert alert-danger", 4, "ngIf"], [1, "checkbox"], ["formControlName", "status", "id", "status", "type", "checkbox", 1, "form-control"], [1, "modal-footer"], ["type", "button", "type", "submit", 1, "btn", 3, "disabled", "ngClass", "click"], [1, "label-control"], [1, "alert", "alert-danger"], ["class", "validation error", 4, "ngIf"], [1, "validation", "error"], [1, "la", "la-check"]],
      template: function ToDoComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "app-lang");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "span", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "span", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "ul", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, ToDoComponent_li_14_Template, 12, 2, "li", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "a", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function ToDoComponent_Template_a_click_15_listener() {
            return ctx.openModal();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "i", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, ToDoComponent_div_17_Template, 24, 15, "div", 12);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.dayNum);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.month);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.year);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.dayName);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("options", ctx.slimScrollopts);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.list);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.open);
        }
      },
      directives: [_lang_lang_component__WEBPACK_IMPORTED_MODULE_4__["LangComponent"], ngx_slimscroll__WEBPACK_IMPORTED_MODULE_5__["SlimScrollDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["CheckboxControlValueAccessor"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgClass"]],
      pipes: [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslatePipe"]],
      styles: ["/****** Start Style List ******/\n.list-container {\n  background-color: #fff;\n  height: 75vh;\n  width: 500px;\n  box-shadow: 0px 6px 15px 0px rgba(207, 211, 218, 0.35);\n  position: relative;\n}\n.list-container .date-details {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  font-size: 16px;\n  margin-bottom: 25px;\n  padding: 70px 50px 0;\n  color: #616473;\n}\n.list-container .date-details .date {\n  display: flex;\n  align-items: center;\n}\n.list-container .date-details .date .day {\n  font-size: 42px;\n  font-family: \"GothamRounded-Meduim\";\n  margin-right: 8px;\n}\n.list-container .date-details .date .details {\n  display: flex;\n  flex-direction: column;\n  font-family: \"GothamRounded-Book\";\n  line-height: 18px;\n}\n.list-container .date-details .date .details .month {\n  font-weight: 600;\n  text-transform: uppercase;\n}\n.list-container .date-details .date .details .year {\n  opacity: 0.33;\n}\n.list-container .date-details .day-name {\n  font-family: \"MyriadPro-Regular\";\n  text-transform: uppercase;\n}\n.list-container ul {\n  height: calc(70vh - 165px);\n  padding: 0 50px;\n}\n.list-container ul li {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n  color: inherit;\n  margin-bottom: 10px;\n  padding: 15px 0;\n}\n.list-container .checkbox {\n  width: calc(100% - 82px);\n  display: flex;\n  justify-content: space-between;\n  flex-direction: row-reverse;\n  align-items: center;\n  margin-right: 8px;\n}\n.list-container .checkbox .check-style {\n  left: auto;\n  right: 0;\n}\n.list-container .checkbox input[type=checkbox] ~ span.text {\n  width: calc(100% - 40px);\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  margin: 0 !important;\n}\n.list-container .action-container .action {\n  display: flex;\n  align-items: center;\n}\n.list-container .action-container .action a {\n  margin-right: 8px;\n}\n.list-container .action-container .action a:last-child {\n  margin-right: 0 !important;\n}\n/****** End List Style ******/\n/****** Start List Responsive Style   ******/\n@media (max-width: 1600px) {\n  .list-container {\n    width: 400px !important;\n  }\n  .list-container ul {\n    padding: 0 35px !important;\n    height: calc(70vh - 100px) !important;\n  }\n  .list-container ul li {\n    margin-bottom: 0 !important;\n    padding: 12px 0 !important;\n  }\n  .list-container .checkbox {\n    width: calc(100% - 72px) !important;\n  }\n  .list-container .date-details {\n    font-size: 14px !important;\n    padding: 50px 35px 0 !important;\n    margin-bottom: 10px !important;\n  }\n  .list-container .date-details .date .day {\n    font-size: 28px !important;\n  }\n  .list-container .change-lang {\n    width: 80px;\n    height: 30px;\n    font-size: 14px;\n  }\n  .list-container .change-lang:hover {\n    width: 90px;\n  }\n  .list-container .slimscroll-bar,\n.list-container .slimscroll-grid {\n    margin: 17.5px !important;\n  }\n}\n@media (max-width: 480px) {\n  .list-container {\n    width: 100% !important;\n    height: calc(100vh - 100px) !important;\n    display: block;\n    margin: auto;\n  }\n  .list-container ul {\n    padding: 0 15px !important;\n    height: calc(90vh - 150px) !important;\n  }\n  .list-container .date-details {\n    padding: 35px 15px 0 !important;\n    font-size: 12px;\n  }\n  .list-container .slimscroll-bar,\n.list-container .slimscroll-grid {\n    margin: 2.5px !important;\n  }\n}\n/****** End List Responsive Style  ******/\n/****** Start List Arabic Style  ******/\nhtml[dir=rtl] .list-container .date-details .date .day {\n  font-size: 42px;\n  font-family: \"GothamRounded-Meduim\";\n  margin-right: 0 !important;\n  margin-left: 8px !important;\n}\nhtml[dir=rtl] .list-container .date-details .date .details {\n  font-family: \"GothamRounded-Book\";\n  line-height: 18px;\n}\nhtml[dir=rtl] .list-container .date-details .date .details .month {\n  font-weight: 600;\n}\nhtml[dir=rtl] .list-container .date-details .day-name {\n  font-family: \"MyriadPro-Regular\";\n}\nhtml[dir=rtl] .list-container .checkbox {\n  margin-right: 0px !important;\n  margin-left: 8px !important;\n}\nhtml[dir=rtl] .list-container .checkbox .check-style {\n  left: 0 !important;\n  right: auto !important;\n}\nhtml[dir=rtl] .list-container .checkbox .action a {\n  margin-right: 0 !important;\n  margin-left: 8px;\n}\nhtml[dir=rtl] .list-container .checkbox .action a:last-child {\n  margin-right: 8px !important;\n  margin-left: 0 !important;\n}\nhtml[dir=rtl] .list-container .action-container .action a {\n  margin-right: 0 !important;\n  margin-left: 8px !important;\n}\nhtml[dir=rtl] .list-container .action-container .action a:last-child {\n  margin-left: 0 !important;\n}\n/****** End List Arabic Style  ******/\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdG8tZG8vRzpcXHRhc2tcXHRvZG8tbGlzdC10YXNrL3NyY1xcYXBwXFx0by1kb1xcdG8tZG8uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3RvLWRvL0c6XFx0YXNrXFx0b2RvLWxpc3QtdGFzay9zcmNcXGFzc2V0c1xcc2Nzc1xcdmFyaWFibGVzLnNjc3MiLCJzcmMvYXBwL3RvLWRvL3RvLWRvLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLCtCQUFBO0FBRUE7RUFDRSxzQkNEVztFREVYLFlBQUE7RUFDQSxZQUFBO0VBQ0Esc0RBQUE7RUFDQSxrQkFBQTtBRURGO0FGR0U7RUFDRSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0EsY0NmWTtBQ2NoQjtBRkdJO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0FFRE47QUZHTTtFQUNFLGVBQUE7RUFDQSxtQ0FBQTtFQUNBLGlCQUFBO0FFRFI7QUZJTTtFQUNFLGFBQUE7RUFDQSxzQkFBQTtFQUNBLGlDQUFBO0VBQ0EsaUJBQUE7QUVGUjtBRktRO0VBQ0UsZ0JBQUE7RUFDQSx5QkFBQTtBRUhWO0FGTVE7RUFDRSxhQUFBO0FFSlY7QUZZSTtFQUNFLGdDQUFBO0VBQ0EseUJBQUE7QUVWTjtBRmVFO0VBQ0UsMEJBQUE7RUFDQSxlQUFBO0FFYko7QUZlSTtFQUNFLGFBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBRWJOO0FGaUJFO0VBQ0Usd0JBQUE7RUFDQSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUVmSjtBRmlCSTtFQUNFLFVBQUE7RUFDQSxRQUFBO0FFZk47QUZrQkk7RUFDRSx3QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtFQUNBLG9CQUFBO0FFaEJOO0FGc0JJO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0FFcEJOO0FGc0JNO0VBQ0UsaUJBQUE7QUVwQlI7QUZzQlE7RUFDRSwwQkFBQTtBRXBCVjtBRjZCQSw2QkFBQTtBQUVBLDRDQUFBO0FBQ0E7RUFDRTtJQUNFLHVCQUFBO0VFM0JGO0VGNkJFO0lBQ0UsMEJBQUE7SUFDQSxxQ0FBQTtFRTNCSjtFRjZCSTtJQUNFLDJCQUFBO0lBQ0EsMEJBQUE7RUUzQk47RUZnQ0U7SUFDRSxtQ0FBQTtFRTlCSjtFRmlDRTtJQUNFLDBCQUFBO0lBQ0EsK0JBQUE7SUFDQSw4QkFBQTtFRS9CSjtFRmtDTTtJQUNFLDBCQUFBO0VFaENSO0VGcUNFO0lBQ0UsV0FBQTtJQUNBLFlBQUE7SUFDQSxlQUFBO0VFbkNKO0VGcUNJO0lBQ0UsV0FBQTtFRW5DTjtFRnVDRTs7SUFFRSx5QkFBQTtFRXJDSjtBQUNGO0FGMENBO0VBQ0U7SUFDRSxzQkFBQTtJQUNBLHNDQUFBO0lBQ0EsY0FBQTtJQUNBLFlBQUE7RUV4Q0Y7RUYwQ0U7SUFDRSwwQkFBQTtJQUNBLHFDQUFBO0VFeENKO0VGNENFO0lBQ0UsK0JBQUE7SUFDQSxlQUFBO0VFMUNKO0VGNkNFOztJQUVFLHdCQUFBO0VFM0NKO0FBQ0Y7QUZpREEseUNBQUE7QUFFQSx1Q0FBQTtBQU1RO0VBQ0UsZUFBQTtFQUNBLG1DQUFBO0VBQ0EsMEJBQUE7RUFDQSwyQkFBQTtBRXJEVjtBRndEUTtFQUNFLGlDQUFBO0VBQ0EsaUJBQUE7QUV0RFY7QUZ3RFU7RUFDRSxnQkFBQTtBRXREWjtBRjJETTtFQUNFLGdDQUFBO0FFekRSO0FGOERJO0VBQ0UsNEJBQUE7RUFDQSwyQkFBQTtBRTVETjtBRjhETTtFQUNFLGtCQUFBO0VBQ0Esc0JBQUE7QUU1RFI7QUZpRVE7RUFDRSwwQkFBQTtFQUNBLGdCQUFBO0FFL0RWO0FGaUVVO0VBQ0UsNEJBQUE7RUFDQSx5QkFBQTtBRS9EWjtBRndFUTtFQUNFLDBCQUFBO0VBQ0EsMkJBQUE7QUV0RVY7QUZ3RVU7RUFDRSx5QkFBQTtBRXRFWjtBRmdGQSxxQ0FBQSIsImZpbGUiOiJzcmMvYXBwL3RvLWRvL3RvLWRvLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCAnLi8uLi8uLi9hc3NldHMvc2Nzcy92YXJpYWJsZXMuc2Nzcyc7XHJcbi8qKioqKiogU3RhcnQgU3R5bGUgTGlzdCAqKioqKiovXHJcblxyXG4ubGlzdC1jb250YWluZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICR3aGl0ZS1jb2xvcjtcclxuICBoZWlnaHQ6IDc1dmg7XHJcbiAgd2lkdGg6IDUwMHB4O1xyXG4gIGJveC1zaGFkb3c6IDBweCA2cHggMTVweCAwcHggcmdiYSgyMDcsIDIxMSwgMjE4LCAwLjM1KTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG4gIC5kYXRlLWRldGFpbHMge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDE2cHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG4gICAgcGFkZGluZzogNzBweCA1MHB4IDA7XHJcbiAgICBjb2xvcjogJHNjb25kYXJ5LWNvbG9yO1xyXG5cclxuICAgIC5kYXRlIHtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHJcbiAgICAgIC5kYXkge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogNDJweDtcclxuICAgICAgICBmb250LWZhbWlseTogJ0dvdGhhbVJvdW5kZWQtTWVkdWltJztcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDhweDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmRldGFpbHMge1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICBmb250LWZhbWlseTogJ0dvdGhhbVJvdW5kZWQtQm9vayc7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IDE4cHg7XHJcblxyXG5cclxuICAgICAgICAubW9udGgge1xyXG4gICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAueWVhciB7XHJcbiAgICAgICAgICBvcGFjaXR5OiAwLjMzO1xyXG5cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcblxyXG4gICAgfVxyXG5cclxuICAgIC5kYXktbmFtZSB7XHJcbiAgICAgIGZvbnQtZmFtaWx5OiBcIk15cmlhZFByby1SZWd1bGFyXCI7XHJcbiAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcblxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgdWwge1xyXG4gICAgaGVpZ2h0OiBjYWxjKDcwdmggLSAxNjVweCk7XHJcbiAgICBwYWRkaW5nOiAwIDUwcHg7XHJcblxyXG4gICAgbGkge1xyXG4gICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIGNvbG9yOiBpbmhlcml0O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgICBwYWRkaW5nOiAxNXB4IDA7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuY2hlY2tib3gge1xyXG4gICAgd2lkdGg6IGNhbGMoMTAwJSAtIDgycHgpO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDhweDtcclxuXHJcbiAgICAuY2hlY2stc3R5bGUge1xyXG4gICAgICBsZWZ0OiBhdXRvO1xyXG4gICAgICByaWdodDogMDtcclxuICAgIH1cclxuXHJcbiAgICBpbnB1dFt0eXBlPWNoZWNrYm94XX5zcGFuLnRleHQge1xyXG4gICAgICB3aWR0aDogY2FsYygxMDAlIC0gNDBweCk7XHJcbiAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xyXG4gICAgICBtYXJnaW46IDAgIWltcG9ydGFudDtcclxuICAgIH1cclxuXHJcbiAgfVxyXG5cclxuICAuYWN0aW9uLWNvbnRhaW5lciB7XHJcbiAgICAuYWN0aW9uIHtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHJcbiAgICAgIGEge1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogOHB4O1xyXG5cclxuICAgICAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAwICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG5cclxuICB9XHJcbn1cclxuXHJcbi8qKioqKiogRW5kIExpc3QgU3R5bGUgKioqKioqL1xyXG5cclxuLyoqKioqKiBTdGFydCBMaXN0IFJlc3BvbnNpdmUgU3R5bGUgICAqKioqKiovXHJcbkBtZWRpYShtYXgtd2lkdGg6MTYwMHB4KSB7XHJcbiAgLmxpc3QtY29udGFpbmVyIHtcclxuICAgIHdpZHRoOiA0MDBweCAhaW1wb3J0YW50O1xyXG5cclxuICAgIHVsIHtcclxuICAgICAgcGFkZGluZzogMCAzNXB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgIGhlaWdodDogY2FsYyg3MHZoIC0gMTAwcHgpICFpbXBvcnRhbnQ7XHJcblxyXG4gICAgICBsaSB7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xyXG4gICAgICAgIHBhZGRpbmc6IDEycHggMCAhaW1wb3J0YW50O1xyXG4gICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC5jaGVja2JveCB7XHJcbiAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSA3MnB4KSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG5cclxuICAgIC5kYXRlLWRldGFpbHMge1xyXG4gICAgICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcclxuICAgICAgcGFkZGluZzogNTBweCAzNXB4IDAgIWltcG9ydGFudDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMTBweCAhaW1wb3J0YW50O1xyXG5cclxuICAgICAgLmRhdGUge1xyXG4gICAgICAgIC5kYXkge1xyXG4gICAgICAgICAgZm9udC1zaXplOiAyOHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmNoYW5nZS1sYW5nIHtcclxuICAgICAgd2lkdGg6IDgwcHg7XHJcbiAgICAgIGhlaWdodDogMzBweDtcclxuICAgICAgZm9udC1zaXplOiAxNHB4O1xyXG5cclxuICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgd2lkdGg6IDkwcHg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAuc2xpbXNjcm9sbC1iYXIsXHJcbiAgICAuc2xpbXNjcm9sbC1ncmlkIHtcclxuICAgICAgbWFyZ2luOiAxNy41cHggIWltcG9ydGFudDtcclxuICAgIH1cclxuXHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1heC13aWR0aDo0ODBweCkge1xyXG4gIC5saXN0LWNvbnRhaW5lciB7XHJcbiAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG4gICAgaGVpZ2h0OiBjYWxjKDEwMHZoIC0gMTAwcHgpICFpbXBvcnRhbnQ7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIG1hcmdpbjogYXV0bztcclxuXHJcbiAgICB1bCB7XHJcbiAgICAgIHBhZGRpbmc6IDAgMTVweCAhaW1wb3J0YW50O1xyXG4gICAgICBoZWlnaHQ6IGNhbGMoOTB2aCAtIDE1MHB4KSAhaW1wb3J0YW50O1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAuZGF0ZS1kZXRhaWxzIHtcclxuICAgICAgcGFkZGluZzogMzVweCAxNXB4IDAgIWltcG9ydGFudDtcclxuICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5zbGltc2Nyb2xsLWJhcixcclxuICAgIC5zbGltc2Nyb2xsLWdyaWQge1xyXG4gICAgICBtYXJnaW46IDIuNXB4ICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuXHJcbn1cclxuXHJcbi8qKioqKiogRW5kIExpc3QgUmVzcG9uc2l2ZSBTdHlsZSAgKioqKioqL1xyXG5cclxuLyoqKioqKiBTdGFydCBMaXN0IEFyYWJpYyBTdHlsZSAgKioqKioqL1xyXG5cclxuaHRtbFtkaXI9XCJydGxcIl0ge1xyXG4gIC5saXN0LWNvbnRhaW5lciB7XHJcbiAgICAuZGF0ZS1kZXRhaWxzIHtcclxuICAgICAgLmRhdGUge1xyXG4gICAgICAgIC5kYXkge1xyXG4gICAgICAgICAgZm9udC1zaXplOiA0MnB4O1xyXG4gICAgICAgICAgZm9udC1mYW1pbHk6ICdHb3RoYW1Sb3VuZGVkLU1lZHVpbSc7XHJcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDAgIWltcG9ydGFudDtcclxuICAgICAgICAgIG1hcmdpbi1sZWZ0OiA4cHggIWltcG9ydGFudDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC5kZXRhaWxzIHtcclxuICAgICAgICAgIGZvbnQtZmFtaWx5OiAnR290aGFtUm91bmRlZC1Cb29rJztcclxuICAgICAgICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xyXG5cclxuICAgICAgICAgIC5tb250aCB7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICAuZGF5LW5hbWUge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBcIk15cmlhZFByby1SZWd1bGFyXCI7XHJcblxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLmNoZWNrYm94IHtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiAwcHggIWltcG9ydGFudDtcclxuICAgICAgbWFyZ2luLWxlZnQ6IDhweCAhaW1wb3J0YW50O1xyXG5cclxuICAgICAgLmNoZWNrLXN0eWxlIHtcclxuICAgICAgICBsZWZ0OiAwICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgcmlnaHQ6IGF1dG8gIWltcG9ydGFudDtcclxuICAgICAgfVxyXG5cclxuICAgICAgLmFjdGlvbiB7XHJcblxyXG4gICAgICAgIGEge1xyXG4gICAgICAgICAgbWFyZ2luLXJpZ2h0OiAwICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICBtYXJnaW4tbGVmdDogOHB4O1xyXG5cclxuICAgICAgICAgICY6bGFzdC1jaGlsZCB7XHJcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogOHB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAwICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC5hY3Rpb24tY29udGFpbmVyIHtcclxuICAgICAgLmFjdGlvbiB7XHJcbiAgICAgICAgYSB7XHJcbiAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDAgIWltcG9ydGFudDtcclxuICAgICAgICAgIG1hcmdpbi1sZWZ0OiA4cHggIWltcG9ydGFudDtcclxuXHJcbiAgICAgICAgICAmOmxhc3QtY2hpbGQge1xyXG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogMCAhaW1wb3J0YW50O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuXHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4vKioqKioqIEVuZCBMaXN0IEFyYWJpYyBTdHlsZSAgKioqKioqL1xyXG4iLCIkbWFpbkJnOiNmMGVmZTk7XHJcbiRtYWluLWNvbG9yOiMzZTQzNGY7XHJcbiRzY29uZGFyeS1jb2xvcjojNjE2NDczO1xyXG4kd2hpdGUtY29sb3I6I2ZmZjtcclxuJHN1Y2Nlc3MtY29sb3I6IzMyOWE2ZTtcclxuJGxpZ2h0LWRlZnVsdC1jb2xvcjojN2U4Mjk5O1xyXG4kbGlnaHQtZGVmdWx0LWJnOiNmM2Y2Zjk7XHJcbiRsaWdodC1wcmltYXJ5LWNvbG9yOiM4OTUwZmM7XHJcbiRkYXJrLXByaW1hcnktY29sb3I6IzY3M2FiNztcclxuJGxpZ2h0LXByaW1hcnktYmc6I2VlZTVmZjtcclxuJGxpZ2h0LWRhbmdlci1jb2xvcjojZjY0ZTYwO1xyXG4kbGlnaHQtZGFuZ2VyLWJnOiNmZmUyZTU7XHJcbiRsaWdodC1zdWNjZXNzLWNvbG9yOnJnYmEoNjIsIDE5MiwgMTM2LCAwLjgpO1xyXG4kbGlnaHQtc3VjY2Vzcy1iZzojYzlmN2Y1O1xyXG4kZGFyay1zdWNjZXNzLWJnOnJnYmEoNjIsIDE5MiwgMTM2LCAwLjgpO1xyXG4kb3ZlcmxheS1iZzpyZ2JhKDAsIDAsIDAsIDAuMjYpO1xyXG4kb3ZlcmxheS1zaGFkb3c6cmdiYSgyMDcsIDIxMSwgMjE4LCAwLjM1KTtcclxuJGJvcmRlci1jb2xvcjojZWVmMGY1O1xyXG4kYm9yZGVyLXNoYWRvdzojZWVmMGY1O1xyXG4kaW5wdXQtY2hlY2tlZC1jb2xvcjojNTBlM2E0O1xyXG4kaW5wdXQtY2hlY2tlZC1zaGFkb3c6IHJnYmEoODAsIDIyNywgMTY0LCAwLjMwKTtcclxuIiwiLyoqKioqKiBTdGFydCBTdHlsZSBMaXN0ICoqKioqKi9cbi5saXN0LWNvbnRhaW5lciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gIGhlaWdodDogNzV2aDtcbiAgd2lkdGg6IDUwMHB4O1xuICBib3gtc2hhZG93OiAwcHggNnB4IDE1cHggMHB4IHJnYmEoMjA3LCAyMTEsIDIxOCwgMC4zNSk7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5saXN0LWNvbnRhaW5lciAuZGF0ZS1kZXRhaWxzIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBmb250LXNpemU6IDE2cHg7XG4gIG1hcmdpbi1ib3R0b206IDI1cHg7XG4gIHBhZGRpbmc6IDcwcHggNTBweCAwO1xuICBjb2xvcjogIzYxNjQ3Mztcbn1cbi5saXN0LWNvbnRhaW5lciAuZGF0ZS1kZXRhaWxzIC5kYXRlIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5saXN0LWNvbnRhaW5lciAuZGF0ZS1kZXRhaWxzIC5kYXRlIC5kYXkge1xuICBmb250LXNpemU6IDQycHg7XG4gIGZvbnQtZmFtaWx5OiBcIkdvdGhhbVJvdW5kZWQtTWVkdWltXCI7XG4gIG1hcmdpbi1yaWdodDogOHB4O1xufVxuLmxpc3QtY29udGFpbmVyIC5kYXRlLWRldGFpbHMgLmRhdGUgLmRldGFpbHMge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBmb250LWZhbWlseTogXCJHb3RoYW1Sb3VuZGVkLUJvb2tcIjtcbiAgbGluZS1oZWlnaHQ6IDE4cHg7XG59XG4ubGlzdC1jb250YWluZXIgLmRhdGUtZGV0YWlscyAuZGF0ZSAuZGV0YWlscyAubW9udGgge1xuICBmb250LXdlaWdodDogNjAwO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuLmxpc3QtY29udGFpbmVyIC5kYXRlLWRldGFpbHMgLmRhdGUgLmRldGFpbHMgLnllYXIge1xuICBvcGFjaXR5OiAwLjMzO1xufVxuLmxpc3QtY29udGFpbmVyIC5kYXRlLWRldGFpbHMgLmRheS1uYW1lIHtcbiAgZm9udC1mYW1pbHk6IFwiTXlyaWFkUHJvLVJlZ3VsYXJcIjtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbi5saXN0LWNvbnRhaW5lciB1bCB7XG4gIGhlaWdodDogY2FsYyg3MHZoIC0gMTY1cHgpO1xuICBwYWRkaW5nOiAwIDUwcHg7XG59XG4ubGlzdC1jb250YWluZXIgdWwgbGkge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGNvbG9yOiBpbmhlcml0O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBwYWRkaW5nOiAxNXB4IDA7XG59XG4ubGlzdC1jb250YWluZXIgLmNoZWNrYm94IHtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDgycHgpO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGZsZXgtZGlyZWN0aW9uOiByb3ctcmV2ZXJzZTtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLXJpZ2h0OiA4cHg7XG59XG4ubGlzdC1jb250YWluZXIgLmNoZWNrYm94IC5jaGVjay1zdHlsZSB7XG4gIGxlZnQ6IGF1dG87XG4gIHJpZ2h0OiAwO1xufVxuLmxpc3QtY29udGFpbmVyIC5jaGVja2JveCBpbnB1dFt0eXBlPWNoZWNrYm94XSB+IHNwYW4udGV4dCB7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSA0MHB4KTtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gIG1hcmdpbjogMCAhaW1wb3J0YW50O1xufVxuLmxpc3QtY29udGFpbmVyIC5hY3Rpb24tY29udGFpbmVyIC5hY3Rpb24ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuLmxpc3QtY29udGFpbmVyIC5hY3Rpb24tY29udGFpbmVyIC5hY3Rpb24gYSB7XG4gIG1hcmdpbi1yaWdodDogOHB4O1xufVxuLmxpc3QtY29udGFpbmVyIC5hY3Rpb24tY29udGFpbmVyIC5hY3Rpb24gYTpsYXN0LWNoaWxkIHtcbiAgbWFyZ2luLXJpZ2h0OiAwICFpbXBvcnRhbnQ7XG59XG5cbi8qKioqKiogRW5kIExpc3QgU3R5bGUgKioqKioqL1xuLyoqKioqKiBTdGFydCBMaXN0IFJlc3BvbnNpdmUgU3R5bGUgICAqKioqKiovXG5AbWVkaWEgKG1heC13aWR0aDogMTYwMHB4KSB7XG4gIC5saXN0LWNvbnRhaW5lciB7XG4gICAgd2lkdGg6IDQwMHB4ICFpbXBvcnRhbnQ7XG4gIH1cbiAgLmxpc3QtY29udGFpbmVyIHVsIHtcbiAgICBwYWRkaW5nOiAwIDM1cHggIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IGNhbGMoNzB2aCAtIDEwMHB4KSAhaW1wb3J0YW50O1xuICB9XG4gIC5saXN0LWNvbnRhaW5lciB1bCBsaSB7XG4gICAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xuICAgIHBhZGRpbmc6IDEycHggMCAhaW1wb3J0YW50O1xuICB9XG4gIC5saXN0LWNvbnRhaW5lciAuY2hlY2tib3gge1xuICAgIHdpZHRoOiBjYWxjKDEwMCUgLSA3MnB4KSAhaW1wb3J0YW50O1xuICB9XG4gIC5saXN0LWNvbnRhaW5lciAuZGF0ZS1kZXRhaWxzIHtcbiAgICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcbiAgICBwYWRkaW5nOiA1MHB4IDM1cHggMCAhaW1wb3J0YW50O1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHggIWltcG9ydGFudDtcbiAgfVxuICAubGlzdC1jb250YWluZXIgLmRhdGUtZGV0YWlscyAuZGF0ZSAuZGF5IHtcbiAgICBmb250LXNpemU6IDI4cHggIWltcG9ydGFudDtcbiAgfVxuICAubGlzdC1jb250YWluZXIgLmNoYW5nZS1sYW5nIHtcbiAgICB3aWR0aDogODBweDtcbiAgICBoZWlnaHQ6IDMwcHg7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICB9XG4gIC5saXN0LWNvbnRhaW5lciAuY2hhbmdlLWxhbmc6aG92ZXIge1xuICAgIHdpZHRoOiA5MHB4O1xuICB9XG4gIC5saXN0LWNvbnRhaW5lciAuc2xpbXNjcm9sbC1iYXIsXG4ubGlzdC1jb250YWluZXIgLnNsaW1zY3JvbGwtZ3JpZCB7XG4gICAgbWFyZ2luOiAxNy41cHggIWltcG9ydGFudDtcbiAgfVxufVxuQG1lZGlhIChtYXgtd2lkdGg6IDQ4MHB4KSB7XG4gIC5saXN0LWNvbnRhaW5lciB7XG4gICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IGNhbGMoMTAwdmggLSAxMDBweCkgIWltcG9ydGFudDtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBtYXJnaW46IGF1dG87XG4gIH1cbiAgLmxpc3QtY29udGFpbmVyIHVsIHtcbiAgICBwYWRkaW5nOiAwIDE1cHggIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IGNhbGMoOTB2aCAtIDE1MHB4KSAhaW1wb3J0YW50O1xuICB9XG4gIC5saXN0LWNvbnRhaW5lciAuZGF0ZS1kZXRhaWxzIHtcbiAgICBwYWRkaW5nOiAzNXB4IDE1cHggMCAhaW1wb3J0YW50O1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgfVxuICAubGlzdC1jb250YWluZXIgLnNsaW1zY3JvbGwtYmFyLFxuLmxpc3QtY29udGFpbmVyIC5zbGltc2Nyb2xsLWdyaWQge1xuICAgIG1hcmdpbjogMi41cHggIWltcG9ydGFudDtcbiAgfVxufVxuLyoqKioqKiBFbmQgTGlzdCBSZXNwb25zaXZlIFN0eWxlICAqKioqKiovXG4vKioqKioqIFN0YXJ0IExpc3QgQXJhYmljIFN0eWxlICAqKioqKiovXG5odG1sW2Rpcj1ydGxdIC5saXN0LWNvbnRhaW5lciAuZGF0ZS1kZXRhaWxzIC5kYXRlIC5kYXkge1xuICBmb250LXNpemU6IDQycHg7XG4gIGZvbnQtZmFtaWx5OiBcIkdvdGhhbVJvdW5kZWQtTWVkdWltXCI7XG4gIG1hcmdpbi1yaWdodDogMCAhaW1wb3J0YW50O1xuICBtYXJnaW4tbGVmdDogOHB4ICFpbXBvcnRhbnQ7XG59XG5odG1sW2Rpcj1ydGxdIC5saXN0LWNvbnRhaW5lciAuZGF0ZS1kZXRhaWxzIC5kYXRlIC5kZXRhaWxzIHtcbiAgZm9udC1mYW1pbHk6IFwiR290aGFtUm91bmRlZC1Cb29rXCI7XG4gIGxpbmUtaGVpZ2h0OiAxOHB4O1xufVxuaHRtbFtkaXI9cnRsXSAubGlzdC1jb250YWluZXIgLmRhdGUtZGV0YWlscyAuZGF0ZSAuZGV0YWlscyAubW9udGgge1xuICBmb250LXdlaWdodDogNjAwO1xufVxuaHRtbFtkaXI9cnRsXSAubGlzdC1jb250YWluZXIgLmRhdGUtZGV0YWlscyAuZGF5LW5hbWUge1xuICBmb250LWZhbWlseTogXCJNeXJpYWRQcm8tUmVndWxhclwiO1xufVxuaHRtbFtkaXI9cnRsXSAubGlzdC1jb250YWluZXIgLmNoZWNrYm94IHtcbiAgbWFyZ2luLXJpZ2h0OiAwcHggIWltcG9ydGFudDtcbiAgbWFyZ2luLWxlZnQ6IDhweCAhaW1wb3J0YW50O1xufVxuaHRtbFtkaXI9cnRsXSAubGlzdC1jb250YWluZXIgLmNoZWNrYm94IC5jaGVjay1zdHlsZSB7XG4gIGxlZnQ6IDAgIWltcG9ydGFudDtcbiAgcmlnaHQ6IGF1dG8gIWltcG9ydGFudDtcbn1cbmh0bWxbZGlyPXJ0bF0gLmxpc3QtY29udGFpbmVyIC5jaGVja2JveCAuYWN0aW9uIGEge1xuICBtYXJnaW4tcmlnaHQ6IDAgIWltcG9ydGFudDtcbiAgbWFyZ2luLWxlZnQ6IDhweDtcbn1cbmh0bWxbZGlyPXJ0bF0gLmxpc3QtY29udGFpbmVyIC5jaGVja2JveCAuYWN0aW9uIGE6bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1yaWdodDogOHB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1sZWZ0OiAwICFpbXBvcnRhbnQ7XG59XG5odG1sW2Rpcj1ydGxdIC5saXN0LWNvbnRhaW5lciAuYWN0aW9uLWNvbnRhaW5lciAuYWN0aW9uIGEge1xuICBtYXJnaW4tcmlnaHQ6IDAgIWltcG9ydGFudDtcbiAgbWFyZ2luLWxlZnQ6IDhweCAhaW1wb3J0YW50O1xufVxuaHRtbFtkaXI9cnRsXSAubGlzdC1jb250YWluZXIgLmFjdGlvbi1jb250YWluZXIgLmFjdGlvbiBhOmxhc3QtY2hpbGQge1xuICBtYXJnaW4tbGVmdDogMCAhaW1wb3J0YW50O1xufVxuXG4vKioqKioqIEVuZCBMaXN0IEFyYWJpYyBTdHlsZSAgKioqKioqLyJdfQ== */"],
      encapsulation: 2
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ToDoComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-to-do',
          templateUrl: './to-do.component.html',
          styleUrls: ['./to-do.component.scss'],
          encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None
        }]
      }], function () {
        return [{
          type: _list_list_service_service__WEBPACK_IMPORTED_MODULE_2__["ListService"]
        }, {
          type: ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/environments/environment.ts":
  /*!*****************************************!*\
    !*** ./src/environments/environment.ts ***!
    \*****************************************/

  /*! exports provided: environment */

  /***/
  function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "environment", function () {
      return environment;
    }); // This file can be replaced during build by using the `fileReplacements` array.
    // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
    // The list of file replacements can be found in `angular.json`.


    var environment = {
      production: false
    };
    /*
     * For easier debugging in development mode, you can import the following file
     * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
     *
     * This import should be commented out in production mode because it will have a negative impact
     * on performance if an error is thrown.
     */
    // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

    /***/
  },

  /***/
  "./src/main.ts":
  /*!*********************!*\
    !*** ./src/main.ts ***!
    \*********************/

  /*! no exports provided */

  /***/
  function srcMainTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./app/app.module */
    "./src/app/app.module.ts");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");

    if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
      Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
    }

    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])["catch"](function (err) {
      return console.error(err);
    });
    /***/

  },

  /***/
  0:
  /*!***************************!*\
    !*** multi ./src/main.ts ***!
    \***************************/

  /*! no static exports found */

  /***/
  function _(module, exports, __webpack_require__) {
    module.exports = __webpack_require__(
    /*! G:\task\todo-list-task\src\main.ts */
    "./src/main.ts");
    /***/
  }
}, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es5.js.map