import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LangComponent } from './../lang/lang.component';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { ToastrService } from 'ngx-toastr';

import { ListService } from '../list/list-service.service';
import { List } from '../list/list.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ToDoComponent implements OnInit {
  list: List[] = [];
  form: FormGroup;
  editable = false;
  open = false;
  editList: any = {};
  id;
  editableIndex;

  slimScrollopts = {
    barBackground: '#B2B8BA',
    barOpacity: '0.8',
    barWidth: '5',
    barBorderRadius: '30',
    barMargin: '25px',
    gridOpacity: '1',
    gridWidth: '5',
    gridBorderRadius: '30',
    gridMargin: '25px',
    alwaysVisible: true,
    visibleTimeout: 1500,
    gridBackground: '#E5E7E8',
  };
  /***** Start Date Variables *****/
  d = new Date();
  month;
  dayName;
  dayNum;
  year;
  monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];
  daysNames = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ];
  /***** End Date Variables *****/
  constructor(public listService: ListService, private toastr: ToastrService) {
    this.month = this.monthNames[this.d.getMonth()];
    this.dayName = this.daysNames[this.d.getDay()];
    this.dayNum = this.d.getUTCDate();
    this.year = this.d.getFullYear();
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      content: new FormControl('', [Validators.required]),
      status: new FormControl(''),
    });
    this.listService.getAll().subscribe((data: List[]) => {
      this.list = data;
      console.log(this.list);
    });
    if (this.editable) {
      this.id = this.editableIndex;
      this.listService.find(this.id).subscribe((data: List) => {
        this.editList = data;
      });
    }
  }
  get f() {
    return this.form.controls;
  }

  /***** Start Add Function *****/
  add() {
    if (!this.editable) {
      this.listService.create(this.form.value).subscribe((res) => {
        this.list.push(res);
        this.toastr.success('Add successfully', '', {
          timeOut: 2000,
        });
      });
    } else {
      this.editable = false;
      this.editList = {
        content: this.editList.content,
        status: this.editList.status,
      };
      this.list.splice(this.editableIndex, 1, this.form.value);
    }
    this.open = false;
    this.form.reset();
  }
  /***** End Add Function *****/

  /***** Start Edit Function *****/
  edit(i, list) {
    this.open = true;
    this.editable = true;
    this.editableIndex = i;
    this.listService.update(i, list).subscribe((res) => {
      this.editList = res;
      this.form.patchValue({
        content: this.editList.content,
        status: this.editList.status,
      });
    });
  }
  /***** End Edit Function *****/

  /***** Start Delete Function *****/
  delete(id) {
    this.listService.delete(id).subscribe((res) => {
      let r = confirm('Are you sure delete item ?');
      if (r === true) {
        this.list = this.list.filter((item) => item.id !== id);
        this.toastr.success('Deleted successfully', '', {
          timeOut: 2000,
        });
      }
    });
  }
  /***** End Add Function *****/

  /***** Start Close Modal Function *****/
  closeModal() {
    this.open = false;
    this.form.reset();
  }
  /***** End Close Modal Function *****/

  /***** Start Open Modal Function *****/
  openModal() {
    this.open = true;
    this.editable = false;
  }
  /***** End Open Modal Function *****/
}
