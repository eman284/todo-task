import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { List } from './list.model';

@Injectable({
  providedIn: 'root',
})
export class ListService {
  private apiURL = 'https://my-json-server.typicode.com/eman284/list-todo';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private httpClient: HttpClient) {}

  getAll(): Observable<List[]> {
    return this.httpClient
      .get<List[]>(this.apiURL + '/list/')
      .pipe(catchError(this.errorHandler));
  }
  create(list): Observable<List> {
    return this.httpClient
      .post<List>(
        this.apiURL + '/list/',
        JSON.stringify(list),
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler));
  }

  find(id): Observable<List> {
    return this.httpClient
      .get<List>(this.apiURL + '/list/' + id)
  }


  update(id, list): Observable<List> {
    return this.httpClient
      .put<List>(
        this.apiURL + '/list/' + id,
        JSON.stringify(list),
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler));
  }
  delete(id) {
    return this.httpClient
      .delete<List>(this.apiURL + '/list/' + id, this.httpOptions)
      .pipe(catchError(this.errorHandler));
  }

  errorHandler(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }
}
