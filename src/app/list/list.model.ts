export interface List {
  id: number;
  content: string;
  status: boolean;
}
