import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-lang',
  templateUrl: './lang.component.html',
  styleUrls: ['./lang.component.scss'],
})
export class LangComponent implements OnInit {
  defaultLang: string;

  constructor(private translate: TranslateService) {}

  ngOnInit(): void {
    this.defaultLang = this.translate.getDefaultLang();
  }

  changeLang(lang: string, dir: string): void {
    this.translate.setDefaultLang(lang);
    document.querySelector('html').setAttribute('dir', dir);
    document.querySelector('html').setAttribute('lang', lang);
    this.defaultLang = lang;
  }
}
